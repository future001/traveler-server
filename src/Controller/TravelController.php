<?php
namespace App\Controller;

use App\Entity\Travel;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use Mhujer\JmsSerializer\Uuid\UuidSerializerHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class TravelController extends Controller
{

    private $serializer;
    private $entityManager;

    public function __construct(SerializerBuilder $serializer, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer::create()
            ->addDefaultHandlers()
            ->configureHandlers(function(HandlerRegistry $registry) {
                $registry->registerSubscribingHandler(new UuidSerializerHandler());
            })
            ->build();
    }

    /**
     * @Route("/api/travels", name="travels", methods={"GET"})
     */
    public function getTravels()
    {
        $travels = $this->getDoctrine()->getRepository(Travel::class)->findBy(["user" => $this->getUser()->getId()]);

        $response = new Response();
        $response->setContent($this->serializer->serialize($travels, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * @Route("/api/travel/{travel}", name="travel", methods={"GET"})
     */
    public function getTravel(Travel $travel)
    {
        $response = new Response();
        if(!$this->isOwner($travel)) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $response->setContent($this->serializer->serialize($travel, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * @Route("/api/travel/add", name="addTravel", methods={"POST"})
     */
    public function addTravel(Request $request)
    {
        $travel = new Travel();
        $travel->setTitle($request->request->get('title'));
        $travel->setUser($this->getUser());
        $travel->setStartDate($request->request->get('startDate'));
        $travel->setEndDate($request->request->get('endDate'));
        
        $this->entityManager->persist($travel);
        $this->entityManager->flush();
        
        $response = new Response();
        $response->setStatusCode(Response::HTTP_CREATED);
        
        return $response;
    }
    
    /**
     * @Route("/api/travel/remove/{travel}", name="removeTravel", methods={"DELETE"})
     */
    public function removeTravel(Travel $travel)
    {
        $response = new Response();
        if(!$this->isOwner($travel)) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $this->entityManager->remove($travel);
        $this->entityManager->flush();
             
        return $response;
    }
    
    private function isOwner(Travel $travel) 
    {
        if($travel->getUser() === $this->getUser()) {
            return true;
        } else {
            return false;
        }
    }
}
