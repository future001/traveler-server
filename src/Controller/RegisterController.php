<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Client;
use Symfony\Component\Routing\Annotation\Route;
use FOS\UserBundle\Model\UserManagerInterface;

class RegisterController extends Controller
{

    private $entityManager;
    private $userManager;
    
    public function __construct(EntityManagerInterface $entityManager, UserManagerInterface $userManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function registerAction(Request $request)
    {
        $email = $request->request->get('email');
        $username = $request->request->get('username');
        $plainPassword = $request->request->get('plainpassword');
        $clientId = $request->request->get('client_id');
        $clientSecret = $request->request->get('client_secret');
        
        $response = new Response();
        $repository = $this->entityManager->getRepository(Client::class);
        
        if(empty($repository->findByIdAndSecret($clientId, $clientSecret))) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
     
        if(null === $email || null === $username || null === $plainPassword) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }
        
        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPlainPassword($plainPassword);
   
        $this->userManager->updateUser($user);
        
        return $response;
    }
}
