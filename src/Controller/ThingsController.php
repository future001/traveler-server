<?php
namespace App\Controller;

use App\Entity\Thing;
use App\Entity\Travel;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use Mhujer\JmsSerializer\Uuid\UuidSerializerHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ThingsController extends Controller
{

    private $serializer;
    private $entityManager;

    public function __construct(SerializerBuilder $serializer, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer::create()
            ->addDefaultHandlers()
            ->configureHandlers(function(HandlerRegistry $registry) {
                $registry->registerSubscribingHandler(new UuidSerializerHandler());
            })
            ->build();
    }

    /**
     * @Route("/api/things/{travel}", name="things", methods={"GET"})
     */
    public function getThings(Travel $travel)
    {
        $response = new Response();
        if(!$this->isOwner($travel)) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $things = $this->getDoctrine()->getRepository(Thing::class)->findBy([ "travel" => $travel ]);

        $response->setContent($this->serializer->serialize($things, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * @Route("/api/things/add/{travel}", name="addThing", methods={"POST"})
     */
    public function addThing(Travel $travel, Request $request)
    {
        $response = new Response();
        if(!$this->isOwner($travel)) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }

        $thing = new Thing();
        $thing->setName($request->request->get('name'));
        $thing->setTravel($travel);
        
        $this->entityManager->persist($thing);
        $this->entityManager->flush();
        
        $response->setStatusCode(Response::HTTP_CREATED);
        
        return $response;
    }

    /**
     * @Route("/api/things/remove/{thing}", name="removeThing", methods={"DELETE"})
     */
    public function removeThing(Thing $thing)
    {
        $response = new Response();
        if(!$this->isOwner($thing->getTravel())) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $this->entityManager->remove($thing);
        $this->entityManager->flush();
        
        return $response;
    }
    
    /**
     * @Route("/api/things/check/{thing}", name="checkThing", methods={"PUT"})
     */
    public function checkThing(Thing $thing)
    {
        $response = new Response();
        if(!$this->isOwner($thing->getTravel())) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $thing->setChecked(true);
        
        $this->entityManager->persist($thing);
        $this->entityManager->flush();
        
        return $response;
    } 
    
    /**
     * @Route("/api/things/uncheck/{thing}", name="uncheckThing", methods={"PUT"})
     */
    public function uncheckThing(Thing $thing)
    {
        $response = new Response();
        if(!$this->isOwner($thing->getTravel())) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $response;
        }
        
        $thing->setChecked(false);
        
        $this->entityManager->persist($thing);
        $this->entityManager->flush();
        
        return $response;
    }  
    
    private function isOwner(Travel $travel) 
    {
        if($travel->getUser() === $this->getUser()) {
            return true;
        } else {
            return false;
        }
    }
}
