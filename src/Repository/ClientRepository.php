<?php
namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Client;


class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }
    
    
    public function findByIdAndSecret($id, $secret): ?Client
    {
        $idParts = explode("_", $id);
        
        if(2 !== count($idParts)) {
            return null;
        }
        
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :id')
            ->andWhere('c.randomId = :randomId')
            ->andWhere('c.secret = :secret')
            ->setParameter('id', $idParts[0])
            ->setParameter('randomId', $idParts[1])
            ->setParameter('secret', $secret)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}