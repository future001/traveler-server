<?php

namespace App\Entity;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="thing")
 * @ORM\Entity(repositoryClass="App\Repository\ThingRepository")
 */
class Thing 
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @JMS\Type("uuid")
	 * @var \Ramsey\Uuid\UuidInterface
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Travel", inversedBy="things")
     * @JMS\Exclude();
     */
    protected $travel;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $checked = false;
    
    function getId(): \Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    function getTravel()
    {
        return $this->travel;
    }

    function getName()
    {
        return $this->name;
    }

    function getChecked()
    {
        return $this->checked;
    }

    function setId(\Ramsey\Uuid\UuidInterface $id)
    {
        $this->id = $id;
    }

    function setTravel($travel)
    {
        $this->travel = $travel;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function setChecked($checked)
    {
        $this->checked = $checked;
    }

}