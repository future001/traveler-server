<?php

namespace App\Entity;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="travel")
 * @ORM\Entity(repositoryClass="App\Repository\TravelRepository")
 */
class Travel 
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     * @JMS\Type("uuid")
	 * @var \Ramsey\Uuid\UuidInterface
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @JMS\Exclude();
     */
    protected $user;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $endDate;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Thing", mappedBy="travel", cascade={"persist", "remove"}) 
     * 
     */    
    protected $things;
    
    function getId(): \Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }
    
    function getUser()
    {
        return $this->user;
    }
    
    function getTitle()
    {
        return $this->title;
    }

    function getStartDate()
    {
        return $this->startDate;
    }

    function getEndDate()
    {
        return $this->endDate;
    }

    function getThings()
    {
        return $this->things;
    }

    function setId(\Ramsey\Uuid\UuidInterface $id)
    {
        $this->id = $id;
    }
    
    function setUser($userId)
    {
        $this->user = $userId;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    function setThings($things)
    {
        $this->things = $things;
    }


}